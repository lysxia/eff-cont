\newcommand\HHcolor{\color{WildStrawberry}}
\newcommand\HH[1]{{\HHcolor#1}}
\newcommand\DDcolor{\color{RoyalBlue}}
\newcommand\DD[1]{{\DDcolor#1}}
\newcommand\TTcolor{\color{PineGreen}}
\newcommand\TT[1]{{\TTcolor#1}}
\newcommand\GG[1]{{\color{gray}#1}}
\newcommand\BB[1]{{\color{black}#1}}
\newcommand\handle[3]{\mathbf{handle}_{#1}\,(#2)\,\mathbf{with}\,(#3)}
\newcommand\handler[2]{\mathbf{handler}_{#1}\,(#2)}
\newcommand\hdo[2]{\mathbf{do}_{#1}\,#2}
\newcommand\reset[2]{\mathbf{reset}_{#1}\,(#2)}
\newcommand\shift[2]{\mathbf{shift}_{#1}\,#2}
\newcommand\dtrans[1]{\DD{\llbracket \HH{#1} \rrbracket^{\HH{\mathbf{h}}}_{\mathbf{d}}}}
\newcommand\dttrans[1]{\TT{\llbracket \DD{#1} \rrbracket^{\DD{\mathbf{d}}}_{\mathbf{ha}}}}
\newcommand\dtransx[1]{\DD{\llbracket \HH{#1} \rrbracket^{\HH{\mathbf{h}}}_{\mathbf{d},2}}}
\newcommand\ttrans[1]{\TT{\llbracket \HH{#1} \rrbracket^{\HH{\mathbf{h}}}_{\mathbf{ha}}}}
\newcommand\trans[1]{\llbracket #1 \rrbracket}
\newcommand\unit{\mathbb{1}}
\newcommand\bool{\mathbb{2}}
\newcommand\typing[6]{#1 \GG{\,|\,} #2 \,|\, #3 \vdash #4 :_{#5} #6}
\newcommand\kinding[3]{#1 \GG{\,|\,} #2 \vdash #3}
\newcommand\llet[3]{\mathbf{let}\,#1=#2\,\mathbf{in}\,#3}
\newcommand\alt{\;|\;}
\newcommand\ctxeq{=_\mathrm{ctx}}

\section{Introduction}

\subsection{Effect handlers}

Effect handlers are a feature to equip programming languages
with user-defined effects. A user may declare their own effect
and define its behavior as a handler.
For example, in Koka, we can declare an effect
\lstinline|read| with one operation \lstinline|ask|.

\begin{lstlisting}
effect read
  ask() : int
\end{lstlisting}

The operation \lstinline|ask| can be called like a function with a unit argument
and a result of type \lstinline|int|.

\begin{lstlisting}
ask() + 1
\end{lstlisting}

That is a computation of type \lstinline|<read> int|, which consists of a
result type \lstinline|int| and an effect row \lstinline|<read>|,
meaning that the computation can only perform operations belonging to the
effects present in the row,
Performing an operation such as \lstinline|ask| is similar to throwing an
exception: control will be passed to a handler on the stack.
Whereas an exception ends the computation delimited by its handler,
effect handlers may resume the computation.
For example, the following handler catches \lstinline|ask| operations, and
resumes execution with the value \lstinline|1| as the result of the operation.

\begin{lstlisting}
handler ctl ask() { resume(1) }
\end{lstlisting}

In Koka, the \lstinline{with} keyword applies a handler to a computation:

\begin{lstlisting}
with handler ctl ask() { resume(1) }
ask() + 1
\end{lstlisting}

This computation has type \lstinline|<> int|. The empty row \lstinline|<>|
means that the computation is pure: it will evaluate to an expression with no
visible effect.
The first evaluation step is to perform the operation \lstinline{ask}.
We find a matching handler above it, the only one in this example.
The context between the handler and the performed operation is the continuation of
that operation: a function which substitutes its argument for the current
operation call \lstinline|ask()|, \lstinline|fun y -> y + 1|.
We substitute that continuation for the \lstinline{resume} keyword in
the body of the handler, \lstinline{resume(1)}, and reduce the resulting expression:
\footnote{For the sake of simplicity, we make the handler shallow in this example.
In Koka as well as in the rest of this paper, handlers are deep.}

\begin{lstlisting}
(fun y -> y + 1)(1)
--> 1 + 1
--> 2
\end{lstlisting}

Effect handlers are a powerful primitive which can express a wide range of
effects, sparing language designers from adding them individually in an ad hoc manner:
exceptions, state, nondeterminism, backtracking, concurrency, etc.

\subsection{Named handlers}

What handler handles an operation? Traditionally, an operation
is handled by the closest matching handler on the stack.
This conventional approach shall be called \emph{dynamically scoped handlers},
or \emph{dynamic handlers} for short.
Dynamic scoping has known weaknesses in abstraction and modularity.
There have been several proposals for
\emph{statically scoped handlers}~\citep{zhang-2019,biernacki-2020,xie-2021,brachthauser-2022},
or \emph{static handlers} to provide stronger guarantees.

With dynamically scoped handlers, it is not possible to predict
what handler handles a given operation in general. Notably, it is not possible
for a library to export effectful functions and guarantee that
their effects are only handled by handlers defined by the library, a failure of
abstraction first identified by~\citet{zhang-2019}. TODO: double check.

Consider again the example of the \lstinline|ask| operation, which lets us
define computations with access to a read-only state. One library may
let you use the \lstinline|ask| operation to read a clock via a handler
\lstinline|readClock|. We may want to write a program which uses
different instances of this effect to compute the time difference between two
clocks. We face a problem: how to specify the clock that each occurrence of
\lstinline|ask| will read? With dynamically scoped handlers,
the closest matching handler shadows further ones. In the following naive program,
both calls to \lstinline|ask| will access the second clock,
which is not the desired behavior.

\begin{lstlisting}
with readClock("Europe/London")
with readClock("Europe/Warsaw")
ask() - ask()
\end{lstlisting}

One early mechanism to disambiguate handlers for the same operations is \emph{masking}:
the \lstinline|mask| combinator hides the closest matching handler for a given effect
from a given computation. Thus the following program computes the difference
between two clocks correctly.

\begin{lstlisting}
with readClock("Europe/London")
with readClock("Europe/Warsaw")
mask<read>(fun () -> ask()) - ask()
\end{lstlisting}

This is a form of DeBruijn indexing: handlers are ordered in a row,
and we specify the one we want by its position. While formally unambiguous,
this feature is not very intuitive to use.

A human-friendlier approach is to give names to
handlers~\citep{zhang-2019,biernacki-2020,xie-2021,brachthauser-2022}.
As our concrete examples are in Koka,
\lyx{This is actually pseudo-Koka: Koka distinguishes named handlers, scoped
handlers, and named scoped handlers. The first can go wrong, the latter two cannot.}
we will start by describing the solution of
\citet{xie-2021}, which adds named handlers to Koka.
Below in \Cref{fig:named-basic}, a variant of the \lstinline|with| construct binds
the handler to a name. We must name the desired handler when performing the
\lstinline|ask| operation.

\begin{figure}[h]
\begin{lstlisting}
with cambridge <- readClock("Europe/London")
with wroclaw   <- readClock("Europe/Warsaw")
cambridge.ask() - wroclaw.ask()
\end{lstlisting}
  \caption{Example program with named handlers}
  \label{fig:named-basic}
\end{figure}

While it is convenient to have names for handlers, we must be careful to
prevent names from escaping their scope: we can only call
\lstinline|cambridge.ask()| when the \lstinline|cambridge| handler is on the
stack. The program below in \Cref{fig:named-bad} would go wrong because it captures a
handler name and uses it after its handler no longer exists.

\begin{figure}[h]
\begin{lstlisting}
let cambridge = (with cambridge <- readClock("Europe/London")
                 cambridge)
with wroclaw <- readClock("Europe/Warsaw")
cambridge.ask() - wroclaw.ask()
\end{lstlisting}
  \caption{Example program with named handlers that goes wrong}
  \label{fig:named-bad}
\end{figure}

\citet{xie-2021} leverage rank-2 polymorphism to manage the scope of names.
We shall call their system FCN (``First class names'' after the title of their paper).
Computations under named handlers take not only a handler name as a parameter,
but also a phantom type variable, called \emph{scope} which tags the effect
that the handler handles.
A handler for an effect \lstinline|e| has a rank-2 type
\lstinline|(forall s. name<e<s>> -> <e<s>,r> t) -> <r> t|.
A handler can be applied to a function which accepts a name in a fresh scope
\lstinline|s| and produces a computation whose effect row includes
the effect \lstinline|e<s>|.
Applying the handler subtracts that signature from the row.
Note that \lstinline|s| cannot occur in \lstinline|t|,
otherwise \lstinline|s| would be unbound in the result type of the handler.

In \Cref{fig:named-basic},
the nested \lstinline|with| expressions desugar to applications of the handlers
to the following function:
\[
  \text{\lstinline|fun cambridge wroclaw -> cambridge.ask() - wroclaw.ask()|,}
\]
which has type
\[
  \text{\lstinline|forall sc sw. name<read<sc>> -> name<read<sc>> -> <read<sc>,read<sw>> int|,}
\]
where the types of the names are tagged with scopes \lstinline|sc| and
\lstinline|sw|. The inner handle subtracts the \lstinline|read<sw>| effect:
\[
  \text{\lstinline|forall sc. name<read<sc>> -> <read<sc>> int|,}
\]
which the outer handle sends to \lstinline{<> int}.
In \Cref{fig:named-bad}, the function \lstinline{fun cambridge -> cambridge}
has type \lstinline|forall sc. name<read<sc>> -> <> name<read<sc>>|, which is not
an appropriate input for a handler. It does not unify \lstinline|forall s. name<e<s>> -> <e<s>,r> t|
because the parameter \lstinline|sc| occurs in the result type of the function.

\citet{biernacki-2020} use a more direct approach: their calculus features a
separate lambda for handler names, with a dependent typing rule to keep track
of the scope of names in the types.
We shall call their system BBD (``Binders by day'').
A handler for an effect \lstinline|e|
has a type of the form \lstinline|(foralln n. <n:e,r> t) -> <r> t|.
A handler is applied to a dependent function which accepts a handler name \lstinline|n| and
produces a computation whose effect row maps the name \lstinline|n| to the effect \lstinline|e|.
Applying the handler subtracts that effect from the row.
In \Cref{fig:named-basic}, the function under the two handlers would have type
\[
  \text{\lstinline{foralln cambridge wroclaw. <cambridge:read,wroclaw:read> int}}
\]
The inner handler subtracts the \lstinline|wroclaw:read| effect:
\[
  \text{\lstinline{foralln cambridge. <cambridge:read> int}}
\]
which the outer handler sends to \lstinline{<> int}.

We subtly switched notations, from Koka's tagging of effects with scopes \lstinline|e<s>|,
to viewing rows as mappings from names to effects \lstinline|n:e|. Formally, this makes little
difference. But we found the latter point of view more intuitive to reconcile FCN and BBD.

In the example that goes wrong in \Cref{fig:named-bad},
the dependent function \lstinline{funname cambridge -> cambridge}
would be ill-typed because names are not first class values in the original BBD:
they cannot be used as expressions.
We could overcome this limitation with singleton types, by allowing names
\lstinline|n| as expressions of type \lstinline|{ n }|,
then that function would have type \lstinline|foralln cambridge. <> { cambridge }|,
which does not unify with \lstinline|foralln n. <n:e,r> t| as \lstinline|n| cannot occur in \lstinline|t|,
so the subsequent application of the handler would not be well-typed.
Even though names are not first-class values in the original BBD,
we can concoct other functions that capture names in the result,
such as \lstinline{funname cambridge -> (fun () -> cambridge.ask())} of
type \lstinline{foralln cambridge. <> (() -> <cambridge:read> int)},
which also does not unify with \lstinline|foralln n. <n:e,r> t|.

Above we can observe some similarities between
\citet{biernacki-2020}'s BBD and \citet{xie-2021}'s FCN, with similar roles
played by the conventional \lstinline|forall| quantifier and the
ad hoc \lstinline|foralln| quantifier.
There are also apparent differences: \lstinline{fun cambridge -> cambridge}
is well-typed by FCN, but not by BBD originally.
In this work, we show that difference to be superficial:
with minor modifications, BBD and FCN are mutually
macro-expressible~\citep{felleisen-1991}.
It is an instance of a well-known translation
of dependent types into a non-dependently typed language
using singleton types~\citep{crary-1998,monnier-2010,eisenberg-2012}.

The main idea is to translate dependent products \lstinline|foralln n. M| in BBD to
polymorphic function types \lstinline|forall (sn : S). name<sn> -> M| in FCN,
viewing \lstinline|name<sn>| as \emph{singleton type}, whose unique inhabitant is a run-time representation of the
scope \lstinline|sn|. Conversely, polymorphism over scopes \lstinline|forall (s : S). M|
in FCN
can be translated to a dependent product over names \lstinline|foralln ns. M| in BBD.
Since names in BBD are available at run time, the FCN type \lstinline|name<s>|
is redundant and can be erased to the unit type \lstinline|()|.

Let us further clarify some nuances about how our work relates to these two.
\citet{xie-2021} actually present two systems: ``named handlers \emph{with}
scoped effects'', and ``named handlers \emph{under} scoped effects'', also
known as \emph{umbrella effects}. Our work concerns the former system.
Adapting our results to umbrella effects is future work.
\citet{biernacki-2020} also present two systems: one with ``open semantics''
is simpler but is restricted to effects with monomorphic operations
such as \lstinline|ask() : int|; the other one with ``generative semantics''
is more elaborate but more general (it is a superset of the former calculus),
allowing for polymorphic operations such as \lstinline|choose<a>(x : a, y : a) : a|,
parameterized by a type \lstinline|a|.
The systems of \citet{xie-2021} use generative semantics, but
could be adapted to use open semantics.
Our work has been formalized with respect to the generative semantics.
\citet{biernacki-2020} have shown that the generative semantics
generalize the open semantics. Assuming that a similar generalization holds
for the system of \citet{xie-2021}, our results imply an equivalence between
the ``open semantics'' variants of the systems, as our translation can be
restricted to the subsets of the calculi with only monomorphic operations.

Our results also further illuminate an aspect of the generative semantics for
statically scoped handlers.
TODO: ask Biernacki et al what they think about this.
Static scoping links operations to their handler at compile time.
However, the generative semantics seem to lose track of this link at run time.
One worrying scenario would be one where two instances of a handler with the same
run-time label occur on the stack.
This situation may happen, see \Cref{fig:duplicate-labels} for example.
In that case, the generative semantics behaves dynamically, choosing the
closest handler with a given label. How can we be sure this is the ``right'' behavior?

\begin{figure}
\begin{lstlisting}
named scoped effect op<s::S>
  ctl op() : (() -> <console> ())

fun main() : <console> ()
  with h <- named handler
              ctl op() { resume(fn () -> resume(fn () -> ())) }
  val f = h.op()
  println("Begin")
  f ()
  println("End")

// Prints:
//     Begin
//     Begin
//     End
//     End
\end{lstlisting}
  \caption{A program which duplicates a handler on the stack.
  The intuition is that with deep handlers, the continuation captures the handler,
  which will be duplicated if the continuation calls itself.
  Our handler captures the continuation and passes it to itself, as \lstinline|f|,
  which the continuation calls.}
  \label{fig:duplicate-labels}
\end{figure}

\citet{biernacki-2020} show that the generative semantics generalize
the open semantics, in which the correspondence between operations and their handlers
is maintained at run time. If we accept the simpler open semantics as being
inherently well-behaved, this implies that monomorphic operations are
still well-behaved in the open semantics. But what of polymorphic operations?
What does it even mean to be ``well-behaved''?

\citet{xie-2021} show that their system does not run into that ambiguity:
a generated handler label occurs at most once on the stack.
However, this invariant relies on the lack of effect subtyping. This rules out
our example \Cref{fig:duplicate-labels}, but the surprising technical reason is
that a pure function \lstinline{a -> <> b} cannot be called in an impure
context \lstinline{a -> <op<s>> b}.
In fact, this limitation is not essential to their system: one can simply allow
subtyping, falling back to the dynamic behavior when multiple occurrences of
a label are on the stack.

In this work, we give strong evidence that the dynamic behavior of the
generative semantics is ``right'', even with polymorphic operations.
Our formalization of the generative
semantics keeps track of effect subtyping coercions precisely,
allowing us to show that when multiple copies of a labeled handler are
on the stack, those beyond the closest one are hidden by a subtyping
coercion, so the closest handler is the only reasonable one to use.

\subsection{Minor design choices}

Comparing BBD and FCN directly is difficult because of several different
choices in their formalizations. Those choices have no significant impact
on our results.
We align those choices to simplify our mechanized formalization of BBD and FCN.
The choices are summarized in \Cref{fig:choices}. Let us explain them in more
detail, starting with the most consequential ones.

\paragraph{Mapping names to signatures}
In BBD,
a name can only be associated to one effect signature---mappings from names
to signatures appear in the contexts of typing judgements.
Function types are indexed by sets of names:
they are of the form $A \to_\epsilon B$ where $\epsilon = a_1, \dots, a_i$.
We call this original variant BBD/U (BBD with unique signatures).
In FCN, different occurrences of the same name can be associated
to different effect signatures---mappings from names to signatures appear in the
types.
Function types are indexed by maps from names to signatures:
arrows are of the form $A \to_\epsilon B$ where $\epsilon = a_1 : \sigma_1, \dots, a_i : \sigma_i$.
We call this original variant FCN/N (FCN with non-unique signatures).
This difference is quite theoretical: a handler still associates a name
to exactly one signature, so there is no actual use for the ability to bind
a name to different signatures in FCN/N.
We may naturally complete the square with BBD/N (BBD with non-unique signatures)
and FCN/U (FCN with unique signatures); see \Cref{fig:square}.
We have formalized only BBD/N and FCN/N for the following three reasons.

First, one technical challenge we would face if we tried to formalize BBD/U and FCN/U
is that these systems are dependently-kinded: types may occur in the context
of types because handler names may occur in types and the context maps them to
types. This kind of dependency is especially challenging to model with an
intrinsically typed representation, which is our approach.
Technically, the syntactic separation of handler names from type variables
in BBD gives us a way out to an intrinsically typed formalization of BBD/U;
unfortunately, the issue persists for FCN/U.
One workaround to dependent kinds that we considered is to introduce a level of
indirection by replacing BBD's first-class signatures with effect names, as in
Koka~\cite{xie-2021}. Effect names would live at the level of kinds, so handler names,
at the type level, could be bound to effect names. The mapping of effect names to operation
types can then be introduced later for the typing of terms.
However, the problem would return if we wanted to model parameterized effects,
such as a state effect parameterized by the type of state.

TODO: EXAMPLES

Second, we believe that there are simple macro-translations in one direction,
from BBD/U to BBD/N and from FCN/U to FCN/N.
We do not know whether type-preserving macro-translations exist in the other
direction, from N to U.
The translations that we conjectured from U to N would only shift the
assignments of signatures to names from the context to the types, without any
change to the syntax relevant to the operational semantics. Our results can
then simply be restricted to an equivalence between BBD/U and FCN/U.

Third, although FCN/N does not gain expressiveness from the non-uniqueness
of signatures, an extension of the calculus could leverage it.
\citet{pottier-2023} present a calculus where the generation of names is
separated from their association to handlers. The same name may be used
effectively with different signatures.
Similarly to our translation from BBD/N to FCN/N,
we conjecture that their calculus could
be reformulated to use rank-2 polymorphism via a singleton translation.

\paragraph{Effect subtyping}
Effect subtyping allows a computation with effect $\epsilon$
to be generalized to any effect $\epsilon'$ which is a superset of $\epsilon$.
In particular, a computation with the empty effect $\iota$ can run in
any context. Effect subtyping is originally present in BBD, but not in FCN.
FCN relies on the lack of effect subtyping to guarantee that handler labels
occur at most once on the stack. This ensures that the semantics is deterministic
without a dynamic search for the ``closest handler''. We will show that this
requirement for labels to be unique can be relaxed: our semantics keeps track
of effect subtyping coercions and maintains the invariant that all but the
closest handler with the same label are hidden by subtyping coercions.

\paragraph{Effect collections}
In BBD, effects are sets of names and support arbitrary union of effects.
In FCN, effects are rows. Rows are less expressive than sets with union,
but are more useful in practice because type inference for rows is well understood.
Our formalization uses rows for simplicity: rows are basically lists and
proofs of row membership are ornamented Peano numbers.

\paragraph{Name quantification in operations}
In BBD, quantifiers for names are separate from quantifiers for types.
Although one may define functions parameterized by names,
name quantification was missing from the syntax of signatures.
Our formalization extends the original system so that operations can be
parameterized by names.
In FCN, names are just a special kind of type, so operations can
naturally be parameterized by names.

\paragraph{First-class signatures}
Our formalization follows BBD in allowing names to be mapped to arbitrary signatures:
effect rows are of the form $a_1 : \sigma_1 , \dots , a_n : \sigma_n$, where $\sigma_i$
is exactly the type of the operation handled by the handler named $a_i$.
In FCN, effect rows are of the form $a_1 : \ell_1,\dots , a_n :\ell_n$,
where $\ell_i$ belong to a global set of ``effect names''. Indeed, in Koka,
effects are given by top-level declarations such as this one:

\begin{lstlisting}
effect read
  ask() : int
\end{lstlisting}

This declares an effect named \lstinline{read},
with one operation \lstinline{ask : () -> int}.
Giving effects separate names from their operations is a useful feature for a
practical language,
For our more theoretical purposes, this unnecessarily complicates our
formalization with an extra level of indirection.

\paragraph{Operation clauses and return clauses}
Traditionally, handlers may handle more than one operation and have a return clause,
inspired by the algebraic view of handlers as folds induced by a free monad.
For simplicity, our handlers consist of a single operation clause, with no return clause.
There is no loss of expressiveness, as the following examples illustrate.

In practice, an effect may consist of multiple operations with different
types of arguments and results; for example, the state effect:

\begin{lstlisting}
effect state
  get() : int
  set(s : int) : ()
\end{lstlisting}

Such effects can be represented in a single polymorphic operation via a
continuation-passing translation.

\begin{lstlisting}
effect state
  getorset<r>(op : (() * (int -> r)) + (int * (() -> r))) : r
\end{lstlisting}

A handler's return clause is applied to the result of the handled computation.
The following example:
%
\begin{lstlisting}
with name <- handler
  ctl ask() { resume(1) }
  return(y) { g(y) }
h name
\end{lstlisting}
%
is equivalent to this expression without a return clause:
%
\begin{lstlisting}
with name <- handler
  ctl ask() { resume(1) }
g (h name)
\end{lstlisting}
%
Note that this relies on effect subtyping:
in the latter version, \lstinline{g} is called under the handler,
but can't actually interact with it since it doesn't have access
to the handler's \lstinline{name}.
%
This transformation is also possible with unnamed handlers,
with the added subtlety that the handler must be masked from the return
clause:
%
\begin{lstlisting}
with handler
  ctl ask() { resume(1) }
  return(y) { g(y) }
h ()
\end{lstlisting}
translates to
\begin{lstlisting}
with handler
  ctl ask(x) { resume(1) }
let y = h ()
mask<read>(fun () -> g(y))
\end{lstlisting}

\paragraph{Typed labels}
We have formalized the generative semantics of static handlers,
which distinguishes compile-time handlers, which bind handler names,
from run-time handlers, which carry labels freshly generated at run time.
\citet{biernacki-2020} give a typing rule for compile-time handlers,
but not run-time handlers. Instead, the necessary invariants for run-time
handlers to not go wrong are defined in a logical relation.
\citet{xie-2021} give typing rules for both compile-time handlers
and run-time handlers. The rule for run-time handlers (cf. rule [EV] in
Figure 7 of \citet{xie-2021}) implicitly relies on a bijection between run-time
labels $m$ and type-level scope parameters $\eta$.
In our formalization, labels are simply substituted for those scope parameters.
Furthermore, their type system does not guarantee that handlers on the stack
all have distinct labels. Instead, this property is proved separately
for a subset of ``handler-safe'' expressions, which are defined semantically
as those expressions reduced from well-typed expressions with no run-time handlers.
We give a static and more general characterization of ``handler-safety''.
Our typing rule for run-time handlers guarantees that the label
does not occur in the rest of the row that is forwarded through the handler,
statically encoding the invariant that any handler further in the stack with
the same label must be hidden by an effect subtyping coercion.

\begin{figure}
  \begin{tabular}{l|c|c|c}
    Feature & BBD & FCN & Our formalization \\
    \hline
    Mapping names to sigs. & in the context & in the types & in the types \\
    Effect subtyping & yes & no & yes \\
    Effect collections & sets & rows & rows \\
    Name quant. in operations & no & yes & yes \\
    First-class signatures & yes & no & yes \\
    Operation clauses & single & multiple & single \\
    Return clause & yes & no & no \\
    Typed labels & no & yes & yes
  \end{tabular}
  \caption{Comparative table of design decisions}
  \label{fig:choices}
\end{figure}

\begin{figure}
\begin{tikzcd}
  \text{BBD/U} & \text{FCN/U} \\
  \text{BBD/N} & \text{FCN/N}
  \arrow[<->, dotted, from=1-1, to=1-2]
  \arrow[->, dotted, from=1-1, to=2-1]
  \arrow[->, dotted, from=1-2, to=2-2]
  \arrow[<->, from=2-1, to=2-2]
\end{tikzcd}
  \caption{Four calculi for lexically scoped handlers: BBD vs FCN style;
  handler names with unique (U) vs non-unique (N) signatures.
  Our technical contribution is the full double arrow between BBD/N and FCN/N.
  The dotted arrows are conjectured macro-translations.}
  \label{fig:square}
\end{figure}
