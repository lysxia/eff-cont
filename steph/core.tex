\section{Introduction}

Which handler handles an operation?

In this experimental draft we study concrete examples to better understand the problem of identifying what handler handles an operation.

\section{Baseline: The obvious case}

The simple situation where it's obvious what handler handles an operation is in
a block of code directly under a handler. For concreteness, examples are
written in Koka, which has all of the relevant features we want to discuss and compare.
%
\begin{lstlisting}
effect ctl print(x : nat) : unit

fun example()
  with some_handler
  print(1)
  print(2)
  print(3)
\end{lstlisting}
%
All of the operations in that block will be handled by the handler \verb|some_handler|.
Formally, handlers are monad morphisms: they commute with sequential composition.
%
\begin{lstlisting}
(* pseudo-Koka TODO make it actual Koka *)
fun example()
  { with some_handler ; print(1) }
  { with some_handler ; print(2) }
  { with some_handler ; print(3) }
\end{lstlisting}

\section{Disambiguating instances}

Sometimes we want to give different interpretations of the same operation.
%
\begin{lstlisting}
effect ctl print(msg : string) : unit

// How to fix this program?
fun example()
  with print_to_stdout
  with print_to_stderr
  print("output")       // this should use the first handler (print_to_stdout)
  print("error")        // this should use the second handler (print_to_stderr)
\end{lstlisting}
%
\subsection{Give the operations different names}
%
\begin{lstlisting}
effect ctl print_out(msg : string) : unit
effect ctl print_err(msg : string) : unit

fun example()
  with print_to_stdout
  with print_to_stderr
  print_out("output")
  print_err("error")
\end{lstlisting}

TODO: Pros and cons

\subsection{Masking}

\begin{lstlisting}
effect ctl print(msg : string) : unit

fun example()
  with print_to_stdout
  with print_to_stderr
  mask<print>(fn() -> print("output"))  // skip the innermost print handler (print_to_stderr)
  print("error")
\end{lstlisting}

\begin{itemize}
  \item Pro: simple to type
  \item Pro: simple to implement
  \item Con: Basically DeBruijn indexing, not human friendly
\end{itemize}

\subsection{Named handlers}

\begin{lstlisting}
named effect ctl print(msg : string) : unit

fun example()
  with stdout <- named handler print_to_stdout
  with stderr <- named handler print_to_stderr
  stdout.print("output")
  stderr.print("error")
\end{lstlisting}

\begin{itemize}
  \item Pro: explicit, readable
  \item Con: (somewhat) tricky to statically prevent names from escaping their scopes
  \item Con: limited expressiveness (TODO: example)
  \item Con: Unclear relationship to the already established "nameless" handlers.
    Seems to require ad hoc semantics.
\end{itemize}

\section{Tracking handlers in higher-order programs}

The above problem is simple. Perhaps too simple to properly compare and evaluate solutions.
Perhaps a more interesting problem must involve higher-order functions.

TODO: this is a simplified example from the Tunneling paper. Here we use a simple exception,
whereas their example uses a Yield effect to actually leverage resumable exceptions.

\subsection{Setup: finding an element in a list}

Goal: given a list \verb|xs : list<a>| and an effectful predicate \verb|f : a -> e bool|,
find an element in the list which satisfies the predicate.

A simple implementation recurses through the list:
%
\begin{lstlisting}
fun find0(xs : list<a>, f : a -> e bool) : e option<a>
  match xs
    Nil -> None
    Cons(x, xs) -> if f(x) then Some(x) else find0(xs, f)
\end{lstlisting}

While simple, that implementation is quite specific to list.

Let us try an approach which is representation-independent:
we will only require the collection to be \emph{iterable}, meaning that it
implements a function \verb|iter| with the following signature:
%
\begin{lstlisting}
fun iter(xs : list<a>, f : a -> e ()) : e ()
  match xs
    Nil -> ()
    Cons(x, xs) -> { f(x) ; iter(xs, f) }
\end{lstlisting}

\subsection{The bad naive solution}

Using \verb|iter|, we can implement \verb|find| by passing a loop body \verb|f' : a -> e ()|
which throws an exception \verb|found(x)| when an element \verb|x| satisfies
the predicate \verb|f|, otherwise \verb|f'| does nothing, so \verb|iter|
continues iterating on the next element.
%
\begin{lstlisting}
effect found<a>
  ctl found(x : a) : b

fun find1(xs : list<a>, f : a -> <found<a>|e> bool) : e option<a>
  with ctl found(x) Some(x)
  fun f'(x) { if f(x) then found(x) }
  iter(xs, f') ; None
\end{lstlisting}

The type of \verb|find1| hints that something is off.
Indeed, the type of the predicate \verb|f : a -> bool|
now mentions the exception effect \verb|found<a>|,
revealing an implementation detail of \verb|find1|, which we would like to be internal.

\verb|find0| has the property that if it returns \verb|Some| value, it must be
an element of the list. \verb|find1| breaks this property: if the effectful
predicate \verb/f : a -> <found<a>|e> bool/ throws a \verb|found(x)|, then
\verb|find1| will just return that element \verb|x|, which may not be in the
given list.

\subsection{Masking}

We must prevent the effectful predicate from raising an exception that will be caught
by the handler in the implementation of \verb|find|. One solution is to use masking:
using \verb|mask<found>|, the handler for \verb|find(x)| is made invisible during
the evaluation of the function call \verb|f(x)|.

\begin{lstlisting}
fun find2(xs : list<a>, f : a -> e bool) : e option<a>
  with ctl found(x) Some(x)
  fun f'(x) { if mask<found>(fn () -> f(x)) then found(x) }
  iter(xs, f') ; None
\end{lstlisting}

TODO: pros and cons

\subsection{Named handlers}

Another approach is to name the handler used in \verb|find|, so that its caller
cannot use it, since it does not have access to the name \verb|h|.

\begin{lstlisting}
named effect found'<a>
  ctl found'(x : a) : b

fun find3(xs : list<a>, f : a -> e bool) : <div,exn|e> option<a>
  with h <- named handler ctl found'(x) Some(x)
  fun f'(x) { if mask<div>(fn() -> mask<exn>(fn() -> f(x)))
              then h.found'(x) }
  iter(xs, f') ; None
\end{lstlisting}

\begin{itemize}
  \item Pro: clear guarantee: you don't have the name, you can't mess with the handler
  \item Con: tricky typing (above we use unscoped names, which is why we have div and exn effects
    arising from handling them)
  \item Con: reduced expressiveness and ad hoc semantics
\end{itemize}
