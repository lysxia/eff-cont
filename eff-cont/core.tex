\newcommand\HHcolor{\color{WildStrawberry}}
\newcommand\HH[1]{{\HHcolor#1}}
\newcommand\DDcolor{\color{RoyalBlue}}
\newcommand\DD[1]{{\DDcolor#1}}
\newcommand\TTcolor{\color{PineGreen}}
\newcommand\TT[1]{{\TTcolor#1}}
\newcommand\GG[1]{{\color{gray}#1}}
\newcommand\BB[1]{{\color{black}#1}}
\newcommand\handle[3]{\mathbf{handle}_{#1}\,(#2)\,\mathbf{with}\,(#3)}
\newcommand\handler[2]{\mathbf{handler}_{#1}\,(#2)}
\newcommand\hdo[2]{\mathbf{do}_{#1}\,#2}
\newcommand\reset[2]{\mathbf{reset}_{#1}\,(#2)}
\newcommand\shift[2]{\mathbf{shift}_{#1}\,#2}
\newcommand\dtrans[1]{\DD{\llbracket \HH{#1} \rrbracket^{\HH{\mathbf{h}}}_{\mathbf{d}}}}
\newcommand\dttrans[1]{\TT{\llbracket \DD{#1} \rrbracket^{\DD{\mathbf{d}}}_{\mathbf{ha}}}}
\newcommand\dtransx[1]{\DD{\llbracket \HH{#1} \rrbracket^{\HH{\mathbf{h}}}_{\mathbf{d},2}}}
\newcommand\ttrans[1]{\TT{\llbracket \HH{#1} \rrbracket^{\HH{\mathbf{h}}}_{\mathbf{ha}}}}
\newcommand\trans[1]{\llbracket #1 \rrbracket}
\newcommand\unit{\mathbb{1}}
\newcommand\bool{\mathbb{2}}
\newcommand\typing[6]{#1 \GG{\,|\,} #2 \,|\, #3 \vdash #4 :_{#5} #6}
\newcommand\kinding[3]{#1 \GG{\,|\,} #2 \vdash #3}
\newcommand\llet[3]{\mathbf{let}\,#1=#2\,\mathbf{in}\,#3}
\newcommand\alt{\;|\;}
\newcommand\ctxeq{=_\mathrm{ctx}}
\newcommand\lift[1]{\mathrm{lift}\,#1}

\section{Introduction}

Effect handlers and delimited control are two closely related approaches to
user-defined effects. Their expressiveness has been studied
recently~\citep{forster-2019,pirog-2019,ikemori-2023} through the
lens of macro expressibility~\citep{felleisen-1991}.

The equivalence between effect handlers and delimited control was first studied
in an untyped setting by \citet{forster-2019}. They also show that the equivalence
is not type-preserving with only simple types, conjecturing that some form of
polymorphism is necessary, which was confirmed by \citet{pirog-2019}.
That equivalence was adapted to lexically scoped effect handlers~\citep{biernacki-2020}
(\Cref{fig:h})
and lexically scoped delimited control by \citet{ikemori-2023} (\Cref{fig:d}).
All of those equivalences were shown to preserve semantics.
In this paper, we investigate the subtleties arising from a stronger notion of
correctness: \emph{full abstraction}.

\subsection{Full abstraction for modular compilation}

Full abstraction can be understood as a compositional variant of semantics preservation.
Simple semantics preservation only provides guarantees about the
translation of a whole program. Imagine compiling an OCaml library using
effect handlers to Wasm using delimited continuations. The compiled library
may be called by a Wasm application which does not originate from the same source language.
Simple semantics preservation says nothing about the execution of such a program.
At the same time, it is not obvious what guarantees we can make in that scenario.
Carrying on, we may refactor the OCaml library to make it more performant,
but without affecting the observable behavior of the library. Formally,
we may assume that the original library and the refactored library are contextually
equivalent. The issue is that the compiled libraries may not be contextually
equivalent: a semantics-preserving compiler (in a simple ``whole-program'' sense)
may not preserve contextual equivalence. The preservation of contextual equivalence
is the property called full abstraction. When a compiler is not fully abstract,
a benign change to a library which is unobservable under the semantics
of the source language, may break an application written in the target
language.

To give another general example, consider a library using parametricity to
enforce and hide internal invariants (\eg{} a binary tree library may enforce
that trees are balanced). Full abstraction implies that even after compilation,
those invariants can't be broken by any application in the target language.

When full abstraction fails, it is because contexts in the target language can observe
changes that would be unobservable in the source language.
To restore full abstraction, our solution will be to extend the target language with
abstraction mechanisms to prevent it from observing what should be unobservable.
In practice, it may not be realistic to actually extend a low-level language
such as Wasm with such features, but our results could be adapted, for example,
into a static analyzer to validate applications that can be safely linked with
a compiled library.

\subsection{Contributions}

We show that the translation
$\dtrans{}$ from handlers to delimited control is not fully abstract:
there exists terms $\HH{M_0}$ and $\HH{M_1}$ that are contextually equivalent using handlers,
$\HH{M_0 \ctxeq M_1}$, but are distinguishable after translation,
$\DD{\dtrans{M_0} \not\ctxeq \dtrans{M_1}}$.
To recover full abstraction, we extend the language of delimited control with
\emph{handler abstractions} (\Cref{fig:ha-sy,fig:ha-ty,fig:ha-op}).
The resulting language is a variant of~\citet{zhang-2019} extended with
polymorphic effects and name abstraction.
Our translation from handlers to delimited control with handler abstractions is
fully abstract.

\subsection{Macro expressibility}

How can we compare the expressiveness of two programming language features?
A challenge is that Turing-complete languages can simulate each other,
so the mere existence of a translation between two languages is not an
informative method of comparison.
A more meaningful tool to measure the expressiveness of programming languages is
macro expressibility~\cite{felleisen-1991}, where we consider only translations
that ``macro expand'' source constructs of interest into a target language.

The main desirable characteristic of macro expansions is compositionality: to
translate an expression, for example $\mathbf{pair}(M,N)$, we first decompose it into
its subterms $M$, $N$, and the root construct $\mathbf{pair}(\cdot_1, \cdot_2)$,
which is a term with holes for its subterms. We then translate each
component separately. The root construct is translated to a term with (labelled) holes
$\trans{\mathbf{pair}(\cdot_1,\cdot_2)}$ in the target language, which we fill
with the translated subterms $\trans{M}$, $\trans{N}$.
\[
  \trans{\mathbf{pair}(M,N)} = \trans{\mathbf{pair}(\cdot_1,\cdot_2)}[\trans{M}][\trans{N}]
\]
For instance, the Chuch encoding of pairs
\(\trans{\mathbf{pair}(M,N)} = \lambda f. f\,\trans{M}\,\trans{N}\)
is a macro expansion which translates the $\mathbf{pair}(\cdot_1,\cdot_2)$
construct to the term $(\lambda f. f\,\cdot_1\,\cdot_2)$.

A language defines a set of \emph{terms} $M$ as well as a set of terms with holes,
or \emph{contexts} $\mathcal{C}$. A term $\mathcal{C}[M]$ is obtained by filling
the one-hole context $\mathcal{C}$ with $M$.
A \emph{compositional translation} between two languages
consists of respective maps between their terms and contexts which commute with
hole substitution:
$$\forall\,\mathcal{C}\,M,\, \trans{\mathcal{C}[M]} = \trans{\mathcal{C}}[\trans{M}].$$
In the rest of this paper, all translations are compositional.

Macro expansions are generally defined between extensions of a common base language,
whose constructs are left unmodified. In this paper, we will focus on extensions
of System F, defined in \Cref{fig:base}. All of our translations $\DD{\trans{}}$
will satisfy the following equations, where expressions in the source language
are colored in \HH{pink} and those in the target language are colored in \DD{blue}.
All that's left to define a translation for an extended language is to give its
mapping for its new constructs.
\begin{align*}
  \DD{\trans{\HH{\lambda\, x.\, M}}} & = \DD{\lambda\, x.\, \trans{\HH{M}}} \\
  \DD{\trans{\HH{M\,N}}} & = \DD{\trans{\HH{M}}\,\trans{\HH{N}}} \\
  \DD{\trans{\HH{x}}} & = \DD{x} \\
  \DD{\trans{\HH{()}}} & = \DD{()} \\
  \DD{\trans{\HH{0}}} & = \DD{0} \\
  \DD{\trans{\HH{1}}} & = \DD{1}
\end{align*}

Booleans serve as observable values in our base language.
A program is a closed term of boolean type $M : \mathbb{2}$,
so that we can observe its value. A basic requirement for a translation
is to preserve the value of programs.

A translation $\DD{\trans{}}$ \emph{preserves semantics} if, for all
well-typed programs $M : \mathbb{2}$ and all booleans $b \in \{0,1\}$,
$$\HH{M \to^\star \BB{b}} \iff \DD{\trans{\HH{M}} \to^\star \BB{b}}.$$
% When the reduction relation $\to$ is deterministic (which is the case in this paper),
% it is sufficient to prove a simpler
% simulation theorem: for any well-typed terms $M$ and $M'$ (of any type),
% $$\HH{M \to M'} \implies \DD{\trans{\HH{M}} \to^\star \trans{\HH{M}}}$$
% That is the property proved by~\citet{forster-2019,pirog-2019,ikemori-2023}

\subsection{Translating handlers to delimited control}

\lyx{Explain effect handlers, delimited control}
We consider the calculus of lexically scoped handlers of \citet{biernacki-2020},
pictured in \Cref{fig:h},
with terms colored in \HH{pink},
and a calculus of delimited control based on the same ideas of lexical scoping as~\citet{biernacki-2020},
pictured in \Cref{fig:d}, with terms colored in \DD{blue};
it is similar to \citet{ikemori-2023}, with a simplified rule for
$\DD{\mathbf{shift}}$ (TODO: which I hope doesn't change the results)
and with name abstraction as in \citet{biernacki-2020}.

\lyx{TODO: check that the usual handlers with multiple operations and return clauses}

\begin{figure}
  \begin{align*}
    x,y,k & \quad\text{Variables} \\
    \alpha,\beta & \quad\text{Type variables} \\
    n & \quad\text{Scope names} \\
    M,N & ::= x \alt M\,N \alt \lambda x.\,M \alt () \alt 0 \alt 1 \\
    A,B,C & ::= \alpha \alt A \to_e B \alt \forall\alpha,\, A \alt \unit \alt \bool \\
    e & ::= \{n_1, \dots, n_k\} \\
    \Delta & ::= \emptyset \alt \Delta, \alpha \\
    \Gamma & ::= \emptyset \alt \Gamma, x : A
  \end{align*}

  \raggedright \fbox{$\kinding{\GG{P\,|\,\Sigma}}{\Delta}{A}$}

  \raggedright \fbox{$\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{M}{e}{A}$}
  \begin{mathpar}
    \inferrule[App]
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{N}{e}{A \to_e B} \\
       \typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{M}{e}{A}}
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{N\,M}{e}{B}}
    \quad
    \inferrule[Lam]
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma, x : A}{M}{e}{B}}
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{\lambda x.\,M}{\{\}}{A \to_e B}}
    \\
    \inferrule[Poly]
      {\typing{\GG{P\,|\,\Sigma}}{\Delta,\alpha}{\Gamma}{M}{e}{A}}
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{M}{e}{\forall \alpha,\,A}}
    \quad
    \inferrule[Mono]
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{M}{e}{\forall \alpha,\,A} \\
       \kinding{\GG{P\,|\,\Sigma}}{\Delta}{B}}
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{M}{e}{A[B/\alpha]}}
    \\
    \inferrule[Var]
      {\Gamma(x) = A}
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{x}{\{\}}{A}}
    \quad
    \inferrule[Unit]
      { }
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{()}{\{\}}{\unit}}
    \quad
    \inferrule[Bool]
      {b \in \{0, 1\}}
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{b}{\{\}}{\bool}}
    \\
    \inferrule[Sub]
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{M}{e}{A} \\
       e \subseteq f \\ A \sqsubseteq B}
      {\typing{\GG{P\,|\,\Sigma}}{\Delta}{\Gamma}{M}{f}{B}}
  \end{mathpar}
  \caption{Base language: System F}
  \label{fig:base}
\end{figure}

\begin{figure}
  \begin{mathpar}
    \inferrule[Sub-$\to$]
      {A' \sqsubseteq A \\ e \subseteq e' \\ B \sqsubseteq B'}
      {A \to_e B \sqsubseteq A' \to_{e'} B'}
  \end{mathpar}
  \caption{Subtyping}
\end{figure}

\begin{figure}
  \[\llet{x}{M}{N} = (\lambda x.\,n)\,M\]
  \caption{Syntactic sugar}
\end{figure}

\begin{figure}
  \begin{align*}
    \HH{M},\HH{N} & ::= \dots \alt \HH{\handle{}{M}{x\,k.\,N}} \alt \HH{\hdo{}{M}} \alt \HH{\lift{M}} \\
    \sigma & ::= \forall \Delta,\,B \Rightarrow C
  \end{align*}

  \raggedright \fbox{$\typing{\HH{\Sigma}}{\Delta}{\Gamma}{\HH{M}}{e}{A}$}
  \begin{mathpar}
    \inferrule[Handle]
      {\kinding{\HH{\Sigma}}{\Delta,\Delta'}{B} \\
       \kinding{\HH{\Sigma}}{\Delta,\Delta'}{C} \\\\
       \typing{\HH{\Sigma}}{\Delta}{\Gamma}{\HH{M}}{e,\HH{\forall \Delta', B \Rightarrow C}}{A} \\
       \typing{\HH{\Sigma}}{\Delta,\Delta'}{\Gamma,\HH{x}:B,\HH{k}: C \to_{e} A}{\HH{N}}{e}{A} }
      {\typing{\HH{\Sigma}}{\Delta}{\Gamma}{\HH{\handle{}{M}{x\,k.\,N}}}{e}{A}}
    \\
    \inferrule[Do]
      {\kinding{\HH{\Sigma}}{\Delta}{\delta : \Delta'} \\
       \typing{\HH{\Sigma}}{\Delta}{\Gamma}{\HH{V}}{\{\}}{B[\delta/\Delta']}}
      {\typing{\HH{\Sigma}}{\Delta}{\Gamma}{\HH{\hdo{}{V}}}{e,\HH{\forall \Delta', B \Rightarrow C}}{C[\delta/\Delta']}}
    \\
    \inferrule[Lift]
      {\typing{\HH{\Sigma}}{\Delta}{\Gamma}{\HH{M}}{e}{A}}
      {\typing{\HH{\Sigma}}{\Delta}{\Gamma}{\HH{\lift{M}}}{e,\sigma}{A}}
  \end{mathpar}

  \raggedright \fbox{$\HH{M\mapsto N}$}
  \begin{align*}
    \HH{
      \handle{}{E[\hdo{}{V}]}{x\,k.\,N}} & \HH{{}\mapsto N[V/x][\lambda y.\,\handle{}{E[y]}{x\,k.\,N}/k]}
  \end{align*}
  \caption{Calculus of handlers \citep{biernacki-2020}}
  \label{fig:h}
\end{figure}

\begin{figure}
  \begin{align*}
    \DD{M},\DD{N} & ::= \dots \alt \DD{\reset{}{M}} \alt \DD{\shift{}{k.\,M}} \alt \DD{\lift{M}} \\
    \rho & ::= \exists \Delta,\,A/e \\
  \end{align*}

  \raggedright \fbox{$\typing{\DD{P}}{\Delta}{\Gamma}{\DD{M}}{e}{A}$}
  \begin{mathpar}
    \inferrule[Reset]
      {\kinding{\DD{P}}{\Delta,\Delta'}{A} \\
       \kinding{\DD{P}}{\Delta,\Delta'}{e} \\
       \kinding{\DD{P}}{\Delta}{\delta : \Delta'} \\\\
       A' = A[\delta/\Delta'] \\
       e' = e[\delta/\Delta'] \\\\
       \typing{\DD{P}}{\Delta}{\Gamma}{\DD{M}}{e',\DD{\exists \Delta', A / e}}{A'}}
      {\typing{\DD{P}}{\Delta}{\Gamma}{\DD{\reset{}{M}}}{e'}{A'}}
    \\
    \inferrule[Shift]
      {\typing{\DD{P}}{\Delta,\Delta'}{\Gamma, \DD{k}:B \to_e A}{\DD{N}}{e}{A}}
      {\typing{\DD{P}}{\Delta}{\Gamma}{\DD{\shift{}{k.\,M}}}{e',\DD{\exists \Delta', A / e}}{B}}
  \end{mathpar}

  \raggedright \fbox{$\DD{M\mapsto N}$}
  \begin{align*}
    \DD{
      \reset{}{E[\shift{}{k.\,M}]}} & \DD{{}\mapsto M[\lambda y.\,\reset{}{E[y]}/k]}
  \end{align*}
  \caption{Calculus of delimited control \citep{ikemori-2023}}
  \label{fig:d}
\end{figure}

The translation from effect handlers to delimited control is denoted by
$\dtrans{}$, defined by the following equations---other constructs are left unchanged:
\begin{align*}
  \dtrans{\handle{n}{M}{x\,k.\,N}} & = \DD{\reset{n}{\llet{x}{\dtrans{M}}{\lambda\,h.\,x}}\, (\lambda x\,k.\,\dtrans{N})} \\
  \dtrans{\hdo{n}{M}} & = \DD{\shift{n}{k.\, \lambda h.\, h\,\dtrans{M}\,(\lambda x.\,k\,x\,h)}}
\end{align*}

A minimal example translating a term which uses a handler and an operation:
\begin{align*}
  & \dtrans{ \handle{n}{E[\hdo{n}{M}]}{x\,k.\,N} } \\
  {}={} &
\DD{
  \reset{n}{\llet{y}{\dtrans{E}[\shift{n}{k.\, \lambda h.\, h\,\dtrans{M}\,(\lambda x.\,k\,x\,h) }]}{\lambda h.\,y}} \, (\lambda x\,k.\,\dtrans{N})}
\end{align*}

\subsection{Breaking abstraction}

Let $\unit$ be the unit type with value $()$ and $\bool$ be the boolean type with values $0$ and $1$.
The following example handles an operation $\unit \to \bool$ by always responding with $1$.
The handled computation calls a function parameter
$\HH{f}$ of type $\HH{\Pi (n : \unit \Rightarrow \bool).\, \unit \to_n \unit}$
and then performs an operation $\HH{\hdo{n}()}$.
$$
\HH{M_0} = \HH{\lambda f.\, \handle{n}{f\, n\, ();\,\hdo{n}{()}}{x\,k.\, k\,1}}
$$

That term is contextually equivalent to the constant function
$$
\HH{M_1} = \HH{\lambda f.\, 1}
$$

However, the translated terms $\dtrans{M_0}$ and $\dtrans{M_1}$
are not contextually equivalent.
Since a translation of $\HH{\mathbf{do}}$ is a $\DD{\mathbf{shift}}$
whose body is a function which accepts the handler as an argument and forwards
it to the continuation,
a malicious context may introduce a $\DD{\mathbf{shift}}$ which doesn't
follow that pattern and instead modifies the handler for the rest of the computation.
Indeed, we can distinguish $\dtrans{M_0}$ and $\dtrans{M_1}$
with the following function as an argument:
$$
\DD{F} = \DD{\lambda n\, x.\, \shift{n}{k.\, \lambda h.\, k\,()\,(\lambda x\,k.\, k\,0)}}
$$
where the handler $h$ is thrown away and replaced with a handler which answers
every operation with $0$.
$$
\DD{\dtrans{M_0}\,F \to^\star 0} \qquad \DD{\dtrans{M_1}\,F \to^\star 1}
$$
In other words, a context in the target language can break abstractions
established in the source language. The translation is not fully abstract.

A similar example appears in~\citet{xie-2020} to motivate a restriction
to \emph{scoped resumptions}: ``a resumption can only be applied under the same handler context as it was captured.''
Under that restriction, they show that their \emph{evidence translation}---to a
calculus similar to \citet{zhang-2019}---preserves semantics. In our case,
effects are lexically scoped, which guarantees that resumptions are scoped.
Our result suggests that evidence translation is also not fully abstract.

\subsection{Full abstraction}

Two terms $\HH{M : A}$ and $\HH{N : A}$ are \emph{contextually equivalent},
denoted $\HH{M \ctxeq N}$, if in
every context $\HH{C : A \to \bool}$, they evaluate to the same value:
$$\forall\,\HH{C},\, \HH{C[M] =_\mathrm{eval} C[N]}$$
where $\HH{C[M] =_\mathrm{eval} C[N]}$ is short for
$
\forall\,\HH{V},\, \HH{C[M] \to^\star V} \iff \HH{C[N] \to^\star V}
$.

If a translation $\DD{\trans{}}$ is compositional and preserves semantics, then
it reflects contextual equivalence:
$$
\forall\, \HH{M\,N},\,\DD{\trans{\HH{M}} \ctxeq \trans{\HH{N}}} \implies \HH{M \ctxeq N}
$$

Proof: we show the contrapositive
$$
\HH{M \not\ctxeq N} \implies \DD{\trans{\HH{M}}\not\ctxeq \trans{\HH{N}}}.
$$
If a context $\HH{C}$ distinguishes $\HH{M}$ and $\HH{N}$, then $\DD{\trans{\HH{C}}}$ distinguishes
$\DD{\trans{\HH{M}}}$ and $\DD{\trans{\HH{N}}}$:
\begin{align*}
  \HH{C[M] \neq_\mathrm{eval} C[N]}
    & \implies \DD{\trans{\HH{C[M]}} \neq_\mathrm{eval} \trans{\HH{C[N]}}} && \text{by semantic preservation} \\
    & \implies \DD{\trans{\HH{C}}[\trans{\HH{M}}] \neq_\mathrm{eval} \trans{\HH{C}}[\trans{\HH{N}}]} && \text{by compositionality}
\end{align*}
\qed

A translation $\DD{\trans{}}$ is \emph{fully abstract}
when it preserves contextual equivalence:
$$
\forall\, \HH{M\,N},\,\HH{M \ctxeq N} \implies \DD{\trans{\HH{M}} \ctxeq \trans{\HH{N}}}
$$

In the language of handlers, all $\HH{\mathbf{do}}$ operations with the same label
are associated to one handler, manipulating the continuation in a uniform way.
In the language of delimited control, every $\DD{\mathbf{shift}}$ may use
the continuation in a different way. The example above shows how we can use
$\DD{\mathbf{shift}}$ to modify a handler. To recover full abstraction, we shall
extend the calculus with a new abstraction mechanism to prevent handlers from
being modified.

\begin{figure}
  \begin{align*}
    \TT{h} & \quad\text{Handler variable} \\
    \TT{M},\TT{N} & ::= \dots \alt \TT{\reset{n}{M}} \alt \TT{\hdo{H}{M}}
      \alt \TT{\lambda n.\, M} \alt \TT{M\,n} \alt \TT{\lambda h.\, M} \alt \TT{M\,H} \\
    \TT{A},\TT{B},\TT{C} & ::= \dots \alt \Pi (\TT{n} : \rho).\, A / e \alt \Pi (\TT{h} : \sigma).\, A / e \\
    \TT{H} & ::= \TT{h} \alt \TT{\handler{n}{x\,k.\,M}} \\
    \TT{e} & ::= \{\epsilon_1, \dots, \epsilon_k\} \\
    \TT{\epsilon} & ::= \TT{n} \alt \TT{h}.\mathrm{lbl} \\
    \rho & ::= \exists \Delta,\,A \\
    \sigma & ::= \forall \Delta,\, B \Rightarrow C \\
    \TT{P} & ::= \TT{\emptyset} \alt \TT{P}, \TT{n} : \rho \\
    \TT{\Sigma} & ::= \TT{\emptyset} \alt \TT{\Sigma}, \TT{h} : \sigma
  \end{align*}
  \caption{Syntax of the calculus of handler abstractions \citep{zhang-2019}}
  \label{fig:ha-sy}
\end{figure}
\begin{figure}
  \begin{mathpar}
    \fbox{$\typing{\TT{P\,|\,\Sigma}}{\Delta}{\Gamma}{\TT{M}}{e}{A}$} \hfill
    \\
    \inferrule[Reset]
      {\kinding{\TT{P \,|\, \Sigma}}{\Delta,\Delta'}{A} \\
       \kinding{\TT{P \,|\, \Sigma}}{\Delta,\Delta'}{e} \\
       \kinding{\TT{P \,|\, \Sigma}}{\Delta}{\delta : \Delta'} \\\\
       A' = A[\delta/\Delta'] \\
       e' = e[\delta/\Delta'] \\\\
       \typing{\TT{P \,|\, \Sigma}}{\Delta}{\Gamma}{\TT{M}}{e',\TT{\exists \Delta', A / e}}{A'}}
      {\typing{\TT{P \,|\, \Sigma}}{\Delta}{\Gamma}{\TT{\reset{}{M}}}{e'}{A'}}
    \\
    \inferrule[HDo]
      {\kinding{\TT{P\,|\,\Sigma}}{\Delta}{\delta : \Delta'} \\
       \typing{\TT{P\,|\,\Sigma}}{\Delta,\Delta'}{\Gamma}{\TT{M}}{\{\}}{B[\delta/\Delta']}}
      {\typing{\TT{P\,|\,\Sigma}}{\Delta}{\Gamma}{\TT{\hdo{}{M}}}{e,\TT{\forall \Delta'.\,\,B \Rightarrow C}}{C[\delta/\Delta']}}
    \\
    \inferrule[HApp]
      {\typing{\TT{P\,|\,\Sigma}}{\Delta}{\Gamma}{\TT{M}}{e,\sigma}{A} \\
       \typing{\TT{P\,|\,\Sigma}}{\Delta}{\Gamma}{\TT{H}}{{}}{\rho \Rightarrow \sigma} }
      {\typing{\TT{P\,|\,\Sigma}}{\Delta}{\Gamma}{\TT{\handle{}{M}{H}}}{e,\rho}{A}}
    \\
    \fbox{$\typing{\TT{P\,|\,\Sigma}}{\Delta}{\Gamma}{\TT{H}}{{}}{\sigma}$} \hfill
    % \\
    % \inferrule[HVar]
    %   {\TT{\Sigma(h)} = \sigma}
    %   {\typing{\TT{P\,|\,\Sigma}}{\Delta}{\Gamma}{\TT{h}}{{}}{\sigma}}
    \\
    \inferrule[Handler]
      {\typing{\TT{P\,|\,\Sigma}}{\Delta,\Delta',\Delta''}{\Gamma, x : B, k : C \to_e A}{\TT{M}}{e}{A}}
      {\typing{\TT{P\,|\,\Sigma}}{\Delta}{\Gamma}{\TT{\handler{}{x\,k.\,M}}}{{}}{(\exists \Delta', A / e) \;\Rightarrow\; (\forall \Delta'', B \Rightarrow C)}}
  \end{mathpar}
  \caption{Typing of the calculus of handler abstractions \citep{zhang-2019}}
  \label{fig:ha-ty}
\end{figure}
\begin{figure}
  \TTcolor
  \begin{align*}
    \reset{}{E[\hdo{}{V}]} & \mapsto M[V/x][\lambda y.\,\reset{}{E[y]}/k] \\
      & \text{where } \handler{}{x\,k.\,M} \in E
  \end{align*}
  \caption{Operational semantics of the calculus of handler abstractions}
  \label{fig:ha-op}
\end{figure}

\subsection{Handler abstraction}

The syntax, typing rules, and operational semantics of handler abstractions
are defined in \Cref{fig:ha-sy,fig:ha-ty,fig:ha-op}, with terms colored
in \TT{green}. This is a slight variant of the calculus of \citet{zhang-2019}
extended with polymorphic effects, which requires a more complex logical relation.
\lyx{double check once the relation has been defined}

The close relation of that calculus with delimited control is evidenced by the
following translation $\dttrans{}$, mapping $\DD{\mathbf{reset}}$ to $\TT{\mathbf{reset}}$
and decomposing $\DD{\mathbf{shift}}$ into a $\TT{\mathbf{do}}$ and
a concrete $\TT{\mathbf{handler}}$.
\begin{align*}
  \dttrans{\reset{n}{M}} & = \TT{\reset{n}{\dttrans{M}}} \\
  \dttrans{\shift{n}{k.\,M}} & = \TT{\hdo{\handler{n}{x\,k.\,\dttrans{M}}}{()}}
\end{align*}

The translation $\ttrans{}$ from handlers to handler abstractions is defined by
the following equations.
A key difference with $\dtrans{}$ is that the handler is bound by a handler abstraction (with variable $\TT{h_n}$)
in the translation of $\HH{\mathbf{handle}}$ instead of a lambda in the translation of $\HH{\mathbf{do}}$.
\begin{align*}
  \ttrans{\handle{n}{M}{x\,k.\,N}} & = \TT{\reset{n}{(\lambda h_{n}.\, \ttrans{M})\,(\handler{n}{x\,k.\,\ttrans{N}})}} \\
  \ttrans{\hdo{n}{M}} & = \TT{\hdo{h_n}{\ttrans{M}}}
\end{align*}

Our main contribution is to prove that $\ttrans{}$ is fully abstract.

\subsection{Another translation}

The differences between $\dtrans{}$ and $\ttrans{}$ suggest another translation
$\dtransx{}$ from handlers to delimited control (without handler abstraction).
\begin{align*}
  \dtransx{\handle{n}{M}{x\,k.\,N}} & = \DD{\reset{n}{(\lambda n.\, \lambda h_{n}.\, \dtransx{M})\,n\,(\lambda x\,k.\,\dtransx{N})}} \\
  \dtransx{\hdo{n}{M}} & = \DD{\shift{n}{k.\, h_n\,\dtransx{M}\,k}}
\end{align*}

\section{Related Work}

\cite{xie-2020}
